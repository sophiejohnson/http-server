/* spidey: Simple HTTP Server */

#include "spidey.h"

#include <errno.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>

/* Global Variables */
char *Port	      = "9898";
char *MimeTypesPath   = "/etc/mime.types";
char *DefaultMimeType = "text/plain";
char *RootPath	      = "www";

/**
 * Display usage message and exit with specified status code.
 *
 * @param   progname    Program Name
 * @param   status      Exit status.
 */
void usage(const char *progname, int status) {
    fprintf(stderr, "Usage: %s [hcmMpr]\n", progname);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -h            Display help message\n");
    fprintf(stderr, "    -c mode       Single or Forking mode\n");
    fprintf(stderr, "    -m path       Path to mimetypes file\n");
    fprintf(stderr, "    -M mimetype   Default mimetype\n");
    fprintf(stderr, "    -p port       Port to listen on\n");
    fprintf(stderr, "    -r path       Root directory\n");
    exit(status);
}

/**
 * Parse command-line options.
 *
 * @param   argc        Number of arguments.
 * @param   argv        Array of argument strings.
 * @param   mode        Pointer to ServerMode variable.
 * @return  true if parsing was successful, false if there was an error.
 *
 * This should set the mode, MimeTypesPath, DefaultMimeType, Port, and RootPath
 * if specified.
 */
bool parse_options(int argc, char *argv[], ServerMode *mode) {
    int argind = 1;
    char *arg;

    while (argind < argc && (arg = argv[argind++])[0] == '-') {
        arg++;
        if (streq(arg, "h")) {
            usage(argv[0], EXIT_SUCCESS);
        }
        else if (streq(arg, "c")) {
            arg = argv[argind++];
            if (streq(arg, "Single")) {
                debug("mode is single\n");
                *mode = SINGLE;
            }
            else if (streq(arg, "Forking")) {
                debug( "mode is forking\n");

                *mode = FORKING;
            }
            else {
                debug("mode unknown\n");
                *mode = UNKNOWN;
            }
        }
        else if (streq(arg, "m")) {
            arg = argv[argind++];
            MimeTypesPath = arg;
        }
        else if (streq(arg, "M")) {
            arg = argv[argind++];
            DefaultMimeType = arg;
        }
        else if (streq(arg, "p")) {
            arg = argv[argind++];
            Port = arg;
        }
        else if (streq(arg, "r")) {
            arg = argv[argind++];
            RootPath = arg;
        }
        else {
            usage(argv[0], EXIT_FAILURE);
        }
    }            
    return true;
}

/**
 * Parses command line options and starts appropriate server
 **/
int main(int argc, char *argv[]) {
    ServerMode mode = SINGLE;

    /* Parse command line options */
    if (argc > 1) {
        parse_options(argc, argv, &mode);
    }

    /* Listen to server socket */
    int socket_fd = -1;
    if ((socket_fd = socket_listen(Port)) < 0) {
        fprintf(stderr, "Socket failure: %s\n", strerror(errno));
        close(socket_fd);
        return EXIT_FAILURE;
    }

    /* Determine real RootPath */
    char buffer[BUFSIZ];
    RootPath = realpath(RootPath, buffer); 
    log("Listening on port %s", Port);
    debug("RootPath        = %s", RootPath);
    debug("MimeTypesPath   = %s", MimeTypesPath);
    debug("DefaultMimeType = %s", DefaultMimeType);
    debug("ConcurrencyMode = %s", mode == SINGLE ? "Single" : "Forking");

    /* Start either forking or single HTTP server */
    switch (mode) {
        case SINGLE:
            single_server(socket_fd);
            break;
        case FORKING:
            forking_server(socket_fd);
            break;
        default:
            fprintf(stderr, "Unidentifiable concurrency mode\n");
            return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */

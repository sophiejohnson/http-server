/* utils.c: spidey utilities */

#include "spidey.h"

#include <ctype.h>
#include <errno.h>
#include <string.h>

#include <sys/stat.h>
#include <unistd.h>

/**
 * Determine mime-type from file extension.
 *
 * @param   path        Path to file.
 * @return  An allocated string containing the mime-type of the specified file.
 *
 * This function first finds the file's extension and then scans the contents
 * of the MimeTypesPath file to determine which mimetype the file has.
 *
 * The MimeTypesPath file (typically /etc/mime.types) consists of rules in the
 * following format:
 *
 *  <MIMETYPE>      <EXT1> <EXT2> ...
 *
 * This function simply checks the file extension version each extension for
 * each mimetype and returns the mimetype on the first match.
 *
 * If no extension exists or no matching mimetype is found, then return
 * DefaultMimeType.
 *
 * This function returns an allocated string that must be free'd.
 **/
char * determine_mimetype(const char *path) {
    char *ext;
    char *mimetype;
    char *token;
    char buffer[BUFSIZ];
    FILE *fs = NULL;

    /* Find file extension */
    ext = strrchr(path, '.');
    debug("extension: %s", ext);
	
    // if no extension exists, return default
    if(!ext){
	debug("Path has no file extension");
        mimetype = strdup(DefaultMimeType);
        return mimetype;
    }
	
    // advance pointer past '.'	
    ext++; 

    /* Open MimeTypesPath file */
    fs = fopen(MimeTypesPath, "r");
    if (!fs) {
 	debug("Could not open mimetypes path: %s", MimeTypesPath);
        fprintf(stderr, "Could not open file %s", strerror(errno));
    }
	
    char * extensions;

    /* Scan file for matching file extensions */
    while(fgets(buffer, BUFSIZ, fs)){
        // separate buffer between mimetype and extensions
        token = strtok(buffer, "\t");
	token = strtok(NULL, "\n");
	if(!token){
	    continue;
	}

	extensions = skip_whitespace(token);
	// check extensions matches ext
	if (streq(ext, extensions)){
	    char * new_token = strtok(buffer, "\t");
            mimetype = strdup(new_token);
            debug("Mimetype: %s", new_token);
            return mimetype;
	}	

	// separate extensions by space, check if each one matches
	extensions = strtok(extensions, " ");
	while(!(streq(token, ""))){
	    if (streq(ext, extensions)){
                char * new_token = strtok(buffer, "\t");
                mimetype = strdup(new_token);
                debug("Mimetype: %s", new_token);
                return mimetype;
            }
	
	    token = strtok(NULL, " ");
	    if(!token){
		break;
	    }
      	    extensions = skip_whitespace(token);
	    if (streq(ext, extensions)){
		char * new_token = strtok(buffer, "\t");
                mimetype = strdup(new_token);
    		debug("Mimetype: %s", new_token);
                	return mimetype;
	    }
	}	
    }
    // if no matching mime type found, return default
    mimetype = strdup(DefaultMimeType);
    return mimetype;
}

/**
 * Determine actual filesystem path based on RootPath and URI.
 *
 * @param   uri         Resource path of URI.
 * @return  An allocated string containing the full path of the resource on the
 * local filesystem.
 *
 * This function uses realpath(3) to generate the realpath of the
 * file requested in the URI.
 *
 * As a security check, if the real path does not begin with the RootPath, then
 * return NULL.
 *
 * Otherwise, return a newly allocated string containing the real path.  This
 * string must later be free'd.
 **/
char * determine_request_path(const char *uri) {

    // Allocate string for path
    char *uri_full = calloc(BUFSIZ, sizeof(char));
    if (uri_full == NULL){
        fprintf(stderr, "Calloc on full uri failed: %s\n", strerror(errno));
        return NULL;
    }
    
    // Find full path of uri
    uri_full = strcpy(uri_full, RootPath);
    if (!streq(uri, "/")) {
        uri_full = strcat(uri_full, uri);
    }

    // Get allocated string for full path
    char *path = calloc(BUFSIZ, sizeof(char));

    if (path == NULL){
        free(uri_full);
        fprintf(stderr, "Calloc on path failed: %s\n", strerror(errno));
        return NULL;
    }
     
    if (!realpath(uri_full, path)){
        free(uri_full);
        free(path);
        fprintf(stderr, "Real path failed: %s\n", strerror(errno));
        return NULL;
    }

    // Check that full path begins with root path
    if(strncmp(path, RootPath, strlen(RootPath)) != 0){
        debug("Real path for uri doesn't match root path");
        free(uri_full);
        free(path);
        return NULL;
    }

    free(uri_full);
    return path;
}

/**
 * Return static string corresponding to HTTP Status code.
 *
 * @param   status      HTTP Status.
 * @return  Corresponding HTTP Status string (or NULL if not present).
 *
 * http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 **/
const char * http_status_string(HTTPStatus status) {

    static char *StatusStrings[] = {
        "200 OK",
        "400 Bad Request",
        "404 Not Found",
        "500 Internal Server Error",
        "418 I'm A Teapot",
    };

    // check status and return corresponding string
    if(status == HTTP_STATUS_OK){
        return StatusStrings[0];
    }
    else if(status == HTTP_STATUS_BAD_REQUEST){
        return StatusStrings[1];
    }
    else if(status == HTTP_STATUS_NOT_FOUND){
        return StatusStrings[2];
    }
    else if(status == HTTP_STATUS_INTERNAL_SERVER_ERROR){
        return StatusStrings[3];
    }

    return NULL;
}

/**
 * Advance string pointer pass all nonwhitespace characters
 *
 * @param   s           String.
 * @return  Point to first whitespace character in s.
 **/
char * skip_nonwhitespace(char *s) {
    while(!(isspace(*s))){
            s++;
    }
    return s;
}

/**
 * Advance string pointer pass all whitespace characters
 *
 * @param   s           String.
 * @return  Point to first non-whitespace character in s.
 **/
char * skip_whitespace(char *s) {
    while(isspace(*s)){
        s++;
    }
    return s;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */


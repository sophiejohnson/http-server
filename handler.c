/* handler.c: HTTP Request Handlers */

#include "spidey.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

/* Internal Declarations */
HTTPStatus handle_browse_request(Request *request);
HTTPStatus handle_file_request(Request *request);
HTTPStatus handle_cgi_request(Request *request);
HTTPStatus handle_error(Request *request, HTTPStatus status);

/**
 * Handle HTTP Request.
 *
 * @param   r           HTTP Request structure
 * @return  Status of the HTTP request.
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
HTTPStatus  handle_request(Request *r) {
    HTTPStatus result;

    /* Parse request */
    if (parse_request(r) < 0) {
        return handle_error(r, HTTP_STATUS_BAD_REQUEST);
    }

    /* Determine request path */
    char *path = determine_request_path(r->uri);
    if (path == NULL) {
        fprintf(stderr, "Unable to find root path%s\n", strerror(errno));
        free(path);
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    r->path = strdup(path);
    if(r->path == NULL){
        free(path);
        return handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
    }

    debug("HTTP REQUEST PATH: %s", r->path);

    /* Dispatch to appropriate request handler type based on file type */
    struct stat s;
    if (stat(r->path, &s) < 0){ 
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    // Directory
    if (S_ISDIR(s.st_mode)) {
        debug("HTTP REQUEST TYPE: BROWSE");
        result = handle_browse_request(r);
    }
    // Executable file
    else if (S_ISREG(s.st_mode) && access(r->path, X_OK) == 0) {
        debug("HTTP REQUEST TYPE: EXECUTABLE");
        result = handle_cgi_request(r);
    }
    // Readable file
    else if (S_ISREG(s.st_mode) && access(r->path, R_OK) == 0) {
        debug("HTTP REQUEST TYPE: FILE");
        result = handle_file_request(r);
    }
    // Error
    else {
        result = handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
    }

    log("HTTP REQUEST STATUS: %s", http_status_string(result));
    return result;
}

/**
 * Handle browse request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP browse request.
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
HTTPStatus  handle_browse_request(Request *r) {
    struct dirent **entries;
    int n;

    /* Open a directory for reading or scanning */
    n = scandir(r->path, &entries, NULL, alphasort);

    if (n < 0) {
        free(entries); 
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    /* Write HTTP Header with OK Status and text/html Content-Type */
    fprintf(r->file, "HTTP/1.0 200 OK\r\n");
    fprintf(r->file, "Content-Type: text/html\r\n");
    fprintf(r->file, "\r\n");
    fprintf(r->file, "<!DOCTYPE html>\r\n");
    fprintf(r->file, "<head>\r\n <title>Systems Final Project</title> \r\n  <meta charset=i\"utf-8\">\r\n <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"> \r\n <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\r\n <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\r\n");
    fprintf(r->file,"<style> body{font: 20px Montserrat, sans-serif; line-height: 1.8; color: #f5f6f7;} \r\n p {font-size: 16px;} .margin {margin-bottom: 45px;}.bg-1 { background-color: #1abc9c; color: #ffffff;}\r\n");
    fprintf(r->file," .container-fluid {padding-top: 70px;padding-bottom: 70px;}\r\n</style>\r\n</head>\r\n");
    /* For each entry in directory, emit HTML list item */
    fprintf(r->file, "<html>\r\n");
    fprintf(r->file, "<div class=\"container-fluid bg-1 text-center\">\r\n");
    fprintf(r->file, "<body>\r\n  <p class=\"bg-primary\"><h1>Welcome to Our Web Server <small> made by Danielle, Katie, and Sophie</small></h1> </p>\r\n");
    fprintf(r->file," <p class=\"bg-primary\"> Choose any of the links below to browse the directories.</p>\r\n");
    fprintf(r->file, "</div? \r\n <ul>\r\n");
    fprintf(r->file, "<div class=\"container\">\r\n");
   
    char message[BUFSIZ] = "Guru!";
    for (int i = 0; i < n; i++) {

        char link[BUFSIZ] = "";
        strcat(link, r->uri); 
        debug("%s", r->uri);

        if (!streq(r->uri, "/")) {
            strcat(link, "/"); 
        }

        debug("%s", link);
        strcat(link, entries[i]->d_name); 

        if (!streq(entries[i]->d_name, ".")) {
            fprintf(r->file, "<li><a href=\"%s\" title=\"Fact:\" data-trigger=\"hover\"\r\n data-content=\"%s\"> %s </li></a>", link, message, entries[i]->d_name);
        }
        
        free(entries[i]);
    }


    fprintf(r->file, "</ul>\r\n");
    fprintf(r->file, "</div>\r\n");
    fprintf(r->file, "<script>\r\n $(document).ready(function(){\r\n $('[data-toggle=\"popover\"]').popover()\r\n;});\r\n</script>\r\n");
    fprintf(r->file, "</body\r\n");
    fprintf(r->file, "</html>\r\n");
    free(entries);
    /* Flush socket, return OK */
    fflush(r->file);
    return HTTP_STATUS_OK;
}

/**
 * Handle file request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
HTTPStatus  handle_file_request(Request *r) {
    debug("Handle file request");
    FILE *fs;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;

    /* Open file for reading */
    debug("Trying to open file %s", r->path);
    fs = fopen(r->path, "r");
    if (!fs) {
        fprintf(stderr, "Could not open file %s: %s", r->path, strerror(errno));
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    /* Determine mimetype */
    debug("Determine mimetype: %s", mimetype);
    mimetype = determine_mimetype(r->path);
    debug("Mimetype: %s", mimetype);

    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->file, "HTTP/1.0 200 OK\r\n");
    fprintf(r->file, "Content-Type: %s\r\n", mimetype);
    fprintf(r->file, "\r\n");

    /* Read from file and write to socket in chunks */
    while (fgets(buffer, BUFSIZ, fs)) {
        fputs(buffer, r->file);
    }

    while ((nread = fread(buffer, 1, BUFSIZ, fs)) > 0) {
        size_t nwritten = fwrite(buffer, 1, nread, r->file);
        while (nwritten != nread) {                 
            nwritten += fwrite(buffer + nwritten, 1, nread - nwritten, fs);
        }
    }
    if (nread < 0) {
        goto fail;
    }

    /* Close file, flush socket, deallocate mimetype, return OK */
    fclose(fs);
    fflush(r->file);
    free(mimetype);
    return HTTP_STATUS_OK;

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    fclose(fs);
    free(mimetype);
    return HTTP_STATUS_INTERNAL_SERVER_ERROR;
}

/**
 * Handle CGI request
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
HTTPStatus handle_cgi_request(Request *r) {
    FILE *pfs;
    char buffer[BUFSIZ];

    /* Export CGI environment variables from globals */
    if(RootPath){
        if(setenv("DOCUMENT_ROOT", RootPath, 1)< 0){
            debug("Setenv of root failed ...%s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    if(Port)
    {
        if(setenv("SERVER_PORT", Port, 1) < 0){
            debug("Setenv of port failed ...%s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }
    /* Export CGI environment variables from request structure:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */

    if(r->query){
        if(setenv("QUERY_STRING", r->query, 1) < 0){
            debug("set_env: query failed... %s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    } else {
        if(setenv("QUERY_STRING", "", 1)< 0){
            debug("set_env: query failed... %s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    if(r->host){
        if(setenv("REMOTE_ADDR", r->host, 1) < 0){
            debug("set_env: host failed... %s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    if(r->port){
        if(setenv("REMOTE_PORT", r->port, 1) < 0){
            debug("set_env: port failed... %s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    if(r->method){
        if(setenv("REQUEST_METHOD", r->method, 1) < 0){
            debug("set_env: method failed... %s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    if(r->uri){
        if(setenv("REQUEST_URI", r->uri, 1) < 0){
            debug("set_env: uri failed... %s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    if(r->path){
        if(setenv("SCRIPT_FILENAME", r->path, 1) < 0){
            debug("set_env: path failed... %s", strerror(errno));
            handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }
    }

    /* Export CGI environment variables from request headers */
    for (Header *h = r->headers; h; h = h->next) {
        if (streq(h->name, "Accept")) {
            setenv("HTTP_ACCEPT", h->value, 1);
        }
        else if (streq(h->name, "Accept-Language")) {
            setenv("HTTP_ACCEPT_LANGUAGE", h->value, 1);
        }
        else if (streq(h->name, "Connection")) {
            setenv("HTTP_CONNECTION", h->value, 1);
        }
        else if (streq(h->name, "Accept-Encoding")) {
            setenv("HTTP_ACCEPT_ENCODING", h->value, 1);
        }
        else if (streq(h->name, "Host")) {
            setenv("HTTP_HOST", h->value, 1);
        }
        else if (streq(h->name, "User-Agent")) {
            setenv("HTTP_USER_AGENT", h->value, 1);
        }
    }

    /* POpen CGI Script */
    pfs = popen(r->path, "r");
    if (!pfs) {
        pclose(pfs);
        return handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
    }

    /* Copy data from popen to socket */
    while (fgets(buffer, BUFSIZ, pfs)) {
        fputs(buffer, r->file);
    }

    /* Close popen, flush socket, return OK */
    pclose(pfs);
    fflush(r->file);
    return HTTP_STATUS_OK;
}

/**
 * Handle displaying error page
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP error request.
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
HTTPStatus  handle_error(Request *r, HTTPStatus status) {
    const char *status_string = http_status_string(status);

    /* Write HTTP Header */
    fprintf(r->file, "HTTP/1.0 %s\r\n", status_string);
    fprintf(r->file, "Content-Type: text/html\r\n");
    fprintf(r->file, "\r\n");

    /* Write HTML Description of Error*/
    fprintf(r->file, "<html>\r\n");
    fprintf(r->file, "<h1>%s</h1>\r\n", status_string);
    fprintf(r->file, "</html>\r\n");
    fprintf(r->file, "Oops! Something went wrong.\r\n");

    /* Return specified status */
    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */

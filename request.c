/* request.c: HTTP Request Functions */

#include "spidey.h"

#include <errno.h>
#include <string.h>

#include <unistd.h>

int parse_request_method(Request *r);
int parse_request_headers(Request *r);

/**
 * Accept request from server socket.
 *
 * @param   sfd         Server socket file descriptor.
 * @return  Newly allocated Request structure.
 *
 * This function does the following:
 *
 *  1. Allocates a request struct initialized to 0.
 *  2. Initializes the headers list in the request struct.
 *  3. Accepts a client connection from the server socket.
 *  4. Looks up the client information and stores it in the request struct.
 *  5. Opens the client socket stream for the request struct.
 *  6. Returns the request struct.
 *
 * The returned request struct must be deallocated using free_request.
 **/
Request* accept_request(int sfd) 
{
    debug("In accept request");
    Request* r;
    struct sockaddr raddr;
    socklen_t rlen = sizeof(raddr);


    /* Allocate request struct (zeroed) */
    debug("Callocing request struct");
    r = calloc(1, sizeof(Request));
    if(r < 0) {
        debug("Calloc failed...");
        goto fail;
    }

    log("Request Struct calloced");

    /* Accept a client */
    debug("about to accept");
    r->fd = accept(sfd, &raddr, &rlen);

    if(r->fd < 0) {
        fprintf(stderr, "%s\n", gai_strerror(errno));
        debug("Unable to accept\n");
        goto fail;
    }
    
    log("Accepted request from %i", r->fd);
    debug("Host: %s", r->host);
    debug("Port: %s", r->port);
    /* Lookup client information */
    int namegotten = getnameinfo(&raddr, rlen, r->host, sizeof(r->host), r->port, sizeof(r->port), NI_NUMERICHOST | NI_NUMERICSERV);
    debug("namegotten = %i", namegotten);

    if(namegotten < 0)
    { 
        debug("Getnameinfo failed... erno = %i strerror =%s", errno,  strerror(errno));
        goto fail;
    }      

    /* Open socket stream */
    r->file = fdopen(r->fd, "w+");

    if(r->file < 0) {
        debug("Unable to open file: %s\n", strerror(errno));
        goto fail;
    }

    log("Accepted request from %s:%s", r->host, r->port);

    return r;

fail:
    /* Deallocate request struct */
    return NULL;
}

/**
 * Deallocate request struct.
 *
 * @param   r           Request structure.
 *
 * This function does the following:
 *
 *  1. Closes the request socket stream or file descriptor.
 *  2. Frees all allocated strings in request struct.
 *  3. Frees all of the headers (including any allocated fields).
 *  4. Frees request struct.
 **/
void free_request(Request* r) {
    /* Close socket or fd */
    debug("close fd");
    if(r->file) {
        fclose(r->file);
    }
    if(r->fd){
        close(r->fd);
    }

    /* Free allocated strings */
    debug("Free method");
    if(r->method){
        free(r->method);
    }
    debug("Free uri");
    if (r->uri) {
        free(r->uri);
    }
    debug("Free path");
    if (r->path) {
        free(r->path);
    }
    debug("Free query");
    if (r->query) {
        free(r->query);
    }

    /* Free headers */
    debug("Free headers");
    Header* temp = r->headers;
    Header* curr = r->headers;

    while(curr != NULL) {
        if(curr->name) {
            free(temp->name);
        }
        if(curr->value) {
            free(temp->value);
        }
        temp = curr->next;
        free(curr);
        curr = temp;
    }

    /* Free request */
    debug("Free requests");
    free(r);
    debug("Out of free function");
}

/**
 * Parse HTTP Request.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * This function first parses the request method, any query, and then the
 * headers, returning 0 on success, and -1 on error.
 **/
int parse_request(Request *r) {

    /* Parse HTTP Request Method */
    debug("Parse request method");
    if (parse_request_method(r) < 0) {
        return -1;
    }

    /* Parse HTTP Request Headers*/
    debug("Parse request headers");
    if (parse_request_headers(r) < 0) {
        return -1;
    }

    return 0;
}

/**
 * Parse HTTP Request Method and URI.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * HTTP Requests come in the form
 *
 *  <METHOD> <URI>[QUERY] HTTP/<VERSION>
 *
 * Examples:
 *
 *  GET / HTTP/1.1
 *  GET /cgi.script?q=foo HTTP/1.0
 *
 * This function extracts the method, uri, and query (if it exists).
 **/
int parse_request_method(Request *r) 
{
    char buffer[BUFSIZ];
    char *method;
    char *uri;
    char *query;

    /* Read line from socket */
    if (fgets(buffer, BUFSIZ, r->file) == NULL) {
        debug("fgets failed from socket\n");
        goto fail;
    }
    /* Parse method and uri */
    method = strtok(buffer, " ");
    if(method == NULL)
    {
        debug("strtok for method failed\n");
        goto fail;
    }

    uri = strtok(NULL, " ");
    if(uri == NULL)
    {
        debug("strtok for uri failed\n");
        goto fail;
    }

    /* Parse query from uri */
    query = strchr(uri, '?');
    if (query) {
        *query = '\0';
        query = query + sizeof(char);
    }

    /* Record method, uri, and query in request struct */
    r->method = strdup(method);
    r->uri = strdup(uri);
    if (query) {
        debug("strdupping query");
        r->query = strdup(query);
    }

    debug("HTTP METHOD: %s", r->method);
    debug("HTTP URI:    %s", r->uri);
    debug("HTTP QUERY:  %s", r->query);

    return 0;

fail:
    return -1;
}

/**
 * Parse HTTP Request Headers.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * HTTP Headers come in the form:
 *
 *  <NAME>: <VALUE>
 *
 * Example:
 *
 *  Host: localhost:8888
 *  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0
 *  Accept: text/html,application/xhtml+xml
 *  Accept-Language: en-US,en;q=0.5
 *  Accept-Encoding: gzip, deflate
 *  Connection: keep-alive
 *
 * This function parses the stream from the request socket using the following
 * pseudo-code:
 *
 *  while (buffer = read_from_socket() and buffer is not empty):
 *      name, value = buffer.split(':')
 *      header      = new Header(name, value)
 *      headers.append(header)
 **/
int parse_request_headers(Request *r) {
    struct header *curr = NULL;
    char buffer[BUFSIZ];
    char *name;
    char *value;

    /* Parse headers from socket */
    while (fgets(buffer, BUFSIZ, r->file) && strlen(buffer) > 2) {

        // Record header name
        chomp(buffer); 
        name = buffer;

        // Record header value
        value = strchr(buffer,':');
        if (value == NULL) {
            debug("strchr for header value failed...\r\n");
            goto fail;
        }
        
        *value = '\0';
        value = value + sizeof(char);
        value = skip_whitespace(value);

        // Create header
        Header *next = calloc(1, sizeof(Header));
        if(!next) {
            debug("calloc on next failed\r\n");
        }

        // Store name and value in header 
        next->name = strdup(name);
        next->value = strdup(value);
        next->next = NULL;

        // If empty header list, make head 
        if (!r->headers) {
            r->headers = next;
            curr = next;
        }
        // Otherwise append to linked list
        else {
            curr->next = next; 
            curr = next;
        }

    }   

#ifndef NDEBUG
    for (Header *h = r->headers; h; h = h->next) {
        debug("HTTP HEADER %s = %s", h->name, h->value);
    }
#endif
    return 0;

fail:
    return -1;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */

/* forking.c: Forking HTTP Server */

#include "spidey.h"

#include <errno.h>
#include <signal.h>
#include <string.h>

#include <unistd.h>

/**
 * Fork incoming HTTP requests to handle the concurrently.
 *
 * @param   sfd         Server socket file descriptor.
 * @return  Exit status of server (EXIT_SUCCESS).
 *
 * The parent should accept a request and then fork off and let the child
 * handle the request.
 **/
int forking_server(int sfd) {

    /* Ignore children */
    signal(SIGCHLD, SIG_IGN);
    
    /* Accept and handle HTTP request */
    while (true) {
    	/* Accept request */
        Request *r = accept_request(sfd);

        if (r) {
            log("Accepted request from %s:%s\n", r->host, r->port);

            /* Fork off child process to handle request */
            pid_t pid = fork();
            // Forking failure
            if (pid < 0) {
                fprintf(stderr, "Failed to fork: %s\n", strerror(errno));
                free_request(r);
                continue; 
            }
            // Child process
            else if (pid == 0) {
                debug("Child process: %d\n", getpid()); 
                close(sfd);
                handle_request(r);
                exit(EXIT_SUCCESS);
            }
            // Parent process
            else {
                free_request(r);
            }
        }
    } 

    /* Close server socket */
    close(sfd);
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */

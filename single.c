/* single.c: Single User HTTP Server */

#include "spidey.h"

#include <errno.h>
#include <string.h>

#include <unistd.h>

/**
 * Handle one HTTP request at a time.
 *
 * @param   sfd         Server socket file descriptor.
 * @return  Exit status of server (EXIT_SUCCESS).
 **/
int single_server(int sfd) {
    /* Accept and handle HTTP request */
    while (true) {
        Request *r;
        HTTPStatus rStat = -1;
        debug("in while true");

        /* Accept request */
        r = accept_request(sfd);

        if (r) {
            /* Handle request */
            log("Accepted request from %s:%s", r->host, r->port);
            rStat = handle_request(r);
            debug("rstat = %i", rStat);
        }

        debug("free request");
        free_request(r);
    }

    debug("Closing server");
    /* Close server socket */
    close(sfd);
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */

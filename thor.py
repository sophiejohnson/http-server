#!/usr/bin/env python3

import multiprocessing
import os
import requests
import sys
import time

# Globals

PROCESSES = 1
REQUESTS  = 1
VERBOSE   = False
URL       = None

# Functions

def usage(status=0):
	print('''Usage: {} [-p PROCESSES -r REQUESTS -v] URL
	    	-h              Display help message
		-v              Display verbose output
	    	-p  PROCESSES   Number of processes to utilize (1)
	    	-r  REQUESTS    Number of requests per process (1)
                '''.format(os.path.basename(sys.argv[0])))
	sys.exit(status)

def do_request(pid):
    ''' Perform REQUESTS HTTP requests and return the average elapsed time. '''
    totTime = 0
    for REQUEST in range(REQUESTS):
            timeStart = time.time()
            response = requests.get(URL)

            if VERBOSE:
                    print(response.text)

            timeEnd = time.time() 
            timeElapsed = timeEnd - timeStart
            totTime = totTime + timeElapsed
            print("Process: {:3}, Request: {:3}, Elapsed Time: {:3}".format(pid, REQUEST, '%.2f' % float(timeElapsed)))
            
    aveTime = totTime / REQUESTS
    print("Process: {:3}, AVERAGE     , Elapsed Time: {:3}".format(pid, '%.2f' % float(aveTime)))

    return aveTime

# Main execution

if __name__ == '__main__':

	# Parse command line arguments
	args = sys.argv[1:]
	while len(args) and args[0].startswith('-') and len(args[0]) > 1:
		arg = args.pop(0)
		if arg == '-h':
			usage(0)
		elif arg == '-v':
			VERBOSE = True
		elif arg == '-p':
			PROCESSES = int(args.pop(0))
		elif arg == '-r':
			REQUESTS = int(args.pop(0))
		else:
				usage(1)
	
	if len(args) == 0:
		usage(1)
	URL = args.pop(0)
	# Create pool of workers and perform requests
	p = multiprocessing.Pool(PROCESSES)
	aveTimeResults = p.map(do_request, range(PROCESSES))

	aveTime = 0
	for time in aveTimeResults:
		aveTime = aveTime + time

	aveTime = aveTime / PROCESSES
	aveTime = '%.2f' % float(aveTime)
	print("TOTAL AVERAGE ELAPSED TIME: ", float(aveTime))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

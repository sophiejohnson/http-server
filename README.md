Project - README
================

- Created a basic HTTP server using system calls in C which allows a client to navigate through directories, view static files, run CGI scripts, and receive error messages
- Executed shell scripts to evaluate throughput and latency of the server in single and forking connection modes

Members
-------

- Kathleen (Katie) Liebscher (kliebsc1@nd.edu)
- Danielle Galvao (dgalvao@nd.edu)
- Sophie Johnson (sjohns37@nd.edu)

Contributions
-------------

| Member        | Contributions |
| ------------- |-------------|
| Danielle  | 1. utils.c <br> 2. debugging <br> 3. handler.c <br> 4. throughput test shell script <br> 5. single.c |
| Katie     | 1. Makefile <br> 2. README.md <br> 3. Part of handler.c <br> 4. request.c <br> 5. thor.py <br> 6. debugging <br> 7. latency test shell script <br> 8. spidey.c|
| Sophie    | 1. single.c <br> 2. debugging <br> 3. handler.c <br> 4. forking.c <br> 5. socket.c <br> 6. spidey.c|
